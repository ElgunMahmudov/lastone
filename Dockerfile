FROM openjdk:17-alpine
COPY build/libs/Ms14App-0.0.1-SNAPSHOT.jar /ms14-app/
CMD ["java", "-jar", "/ms14-app/Ms14App-0.0.1-SNAPSHOT.jar"]