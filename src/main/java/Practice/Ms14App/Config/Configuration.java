package Practice.Ms14App.Config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

@org.springframework.context.annotation.Configuration
public class  Configuration {
    @Bean
    public ModelMapper getModelMapper(){
        return  new ModelMapper();
    }

}
