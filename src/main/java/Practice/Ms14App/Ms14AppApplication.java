package Practice.Ms14App;

import Practice.Ms14App.Config.AppConfig;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class  Ms14AppApplication implements CommandLineRunner {
//	@Value("${ms.name}")
//	private String name;
//	@Value("${ms.teacher}")
//	private String teacher;
	private final AppConfig appConfig;

	public static void main(String[] args) {
		SpringApplication.run(Ms14AppApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("MS nane is " + appConfig.getName());
		System.out.println("ms teacher is " + appConfig.getTeacher());
		System.out.println("ms students is" +appConfig.getStudents() );

	}

}

