package Practice.Ms14App.Dto;

import lombok.Data;

@Data
public class StudentDto {
    Integer id;
            String name;
            String surname;
}
