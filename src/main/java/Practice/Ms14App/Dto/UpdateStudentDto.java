package Practice.Ms14App.Dto;

import lombok.Data;

@Data
public class UpdateStudentDto {
    Integer id;
    String name;
    String surname;
}
