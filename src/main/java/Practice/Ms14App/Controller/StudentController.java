package Practice.Ms14App.Controller;

import Practice.Ms14App.Dto.CreateStudentDto;
import Practice.Ms14App.Dto.StudentDto;
import Practice.Ms14App.Dto.UpdateStudentDto;
import Practice.Ms14App.Service.StudentService;
import Practice.Ms14App.model.Student;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/student")
public class StudentController  {
    private final StudentService studentService;
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }
    @PostMapping
    public void create(@RequestBody CreateStudentDto student){
studentService.create(student);
    }
    public void update(@RequestBody UpdateStudentDto student){
studentService.update(student);
    }
    @GetMapping("{id}")
    public StudentDto get(@PathVariable Integer id){
return studentService.get(id);

    }
@RequestMapping(value="/{id}",method = RequestMethod.DELETE)
    public void delete(@PathVariable Integer id,
                       @RequestParam String page,
                       @RequestHeader String lang) {
        studentService.delete(id);
    }
}
