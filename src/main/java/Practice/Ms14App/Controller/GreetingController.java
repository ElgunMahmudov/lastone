package Practice.Ms14App.Controller;

import Practice.Ms14App.model.Student;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/ms14")
public class GreetingController {
    @GetMapping
    public String sayHello(@RequestParam String lang) {
        switch (lang) {
            case "en":
                return "hello from ms14";
            case "az":
                return "ms14den salam";
            case "ru":
                return "privet iz ms14";
            default:
                return "i dont understand";
        }
    }
    @PostMapping
    public String create(@RequestBody Student student) {
        System.out.println("Student name is" + student.getName());
        System.out.println("Student surname is"  + student.getSurname());
        System.out.println("Student id is" +   student.getId());
        return "hello" + student.getName();
    }
@PutMapping
    public void updateStudent(@RequestBody Student student){
    System.out.println("Student updated");
}
@DeleteMapping("/{id}")
public String deleteStudent(@PathVariable Integer id){
    return "Student with id :" + id + "deleted successfully";
}
    @PostMapping("/register")
    public void register(@RequestBody Register register){
        if(!register.getPassword().equals(register.getRepeatPassword())){
            throw new RuntimeException();
        }
        System.out.println("User with email" + register.getEmail() + "succesfully created");
    }
}
