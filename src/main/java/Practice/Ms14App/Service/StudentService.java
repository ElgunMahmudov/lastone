package Practice.Ms14App.Service;

import Practice.Ms14App.Dto.CreateStudentDto;
import Practice.Ms14App.Dto.StudentDto;
import Practice.Ms14App.Dto.UpdateStudentDto;
import Practice.Ms14App.model.Student;
import Practice.Ms14App.Repository.StudentRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentService {
    private final StudentRepository studentRepository;
    private final ModelMapper modelMapper;

    public StudentService(StudentRepository studentRepository,ModelMapper modelMapper) {
        this.studentRepository = studentRepository;
        this.modelMapper=modelMapper;
    }

    public void create(CreateStudentDto  dto) {
//        Student student = new Student();
//        student.setName(dto.getName());
//        student.setSurname(dto.getSurname());
//        student.setPassWord(dto.getPassWord());
        Student student =  modelMapper.map(dto,Student.class);
        studentRepository.save(student);
    }


    public void update(UpdateStudentDto student) {
        Optional<Student> entity = studentRepository.findById(student.getId());
        entity.ifPresent(student1 -> {
            student1.setName(student.getName());
            student1.setSurname(student.getSurname());
            studentRepository.save(student1);
        });
    }
    public List<Student> getAll(){
      return   studentRepository.findAll();
    }

    public StudentDto get(Integer id) {
        Student student = studentRepository.findById(id).get();
//        StudentDto studentDto =new StudentDto();
//        studentDto.setId(student.getId());
//        studentDto.setName(student.getName());
//        studentDto.setSurname(student.getSurname());
       StudentDto studentDto= modelMapper.map(student,StudentDto.class);
        return studentDto;
    }

    public void delete(Integer id) {
        studentRepository.deleteById(id);
    }
}
