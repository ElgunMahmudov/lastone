package Practice.Ms14App.Service;

import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope("prototype")
public class TeacherServiceImpl implements TeacherService{
}
