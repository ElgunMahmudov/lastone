package Practice.Ms14App.Repository;

import Practice.Ms14App.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student,Integer> {
}
